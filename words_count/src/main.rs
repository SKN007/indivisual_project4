use lambda_runtime::{handler_fn, Context, Error};
use serde_json::json;
use serde_json::Value;

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    let input = event["s1-output"].as_str().unwrap_or_default();
    let word_count = input.split_whitespace().count();
    Ok(json!({ "output": word_count }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(function_handler)).await?;
    Ok(())
}
