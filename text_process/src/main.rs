use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{json, Value};
use std::collections::HashSet;

// A simple list of English stopwords
fn stopwords() -> HashSet<&'static str> {
    let words = vec![
        "a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "as", "at",
        "be", "because", "been", "before", "being", "below", "between", "both", "but", "by",
        "could",
        "did", "do", "does", "doing", "down", "during",
        "each",
        "few", "for", "from", "further",
        "had", "has", "have", "having", "he", "her", "here", "hers", "herself", "him", "himself", "his", "how",
        "i", "if", "in", "into", "is", "it", "its", "itself",
        "just",
        "of", "off", "on", "once", "only", "or", "other", "our", "ours", "ourselves", "out", "over", "own",
        "same", "she", "should", "so", "some", "such",
        "than", "that", "the", "their", "theirs", "them", "themselves", "then", "there", "these", "they", "this", "those", "through", "to", "too",
        "under", "until", "up",
        "very",
        "was", "we", "were", "what", "when", "where", "which", "while", "who", "whom", "why", "will", "with", "would",
        "you", "your", "yours", "yourself", "yourselves"
    ];
    words.into_iter().collect()
}

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    let input = event["input"].as_str().unwrap_or_default().to_lowercase();
    let stop_words = stopwords();
    let filtered_input = input.split_whitespace()
        .filter(|word| !stop_words.contains(word))
        .collect::<Vec<&str>>()
        .join(" ");
    Ok(json!({ "s1-output": filtered_input }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(function_handler)).await?;
    Ok(())
}