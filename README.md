[![pipeline status](https://gitlab.com/SKN007/indivisual_project4/badges/main/pipeline.svg)](https://gitlab.com/SKN007/indivisual_project4/-/commits/main)

# Individual Project 4

## Requirements

 Rust AWS Lambda function

Step Functions workflow coordinating Lambdas

Orchestrate data processing pipeline

## Steps

1. Create some lambda functions. I created two.

   ```
   cargo lambda new text_process
   cargo lambda new words_count
   ```
2. Implement all the functions in src/main.rs. The first one, *text_process*, is used to convert the all the words in the input text to lowercase and then remove all stopwords. The second one, *words_count*, is used to count the number of the input text.
3. Build and deploy

   ```
   cargo lambda build
   cargo lambda deploy
   ```
4. In AWS console. go to Step Functions. Create a state machine.
5. In the state machine, customize the process and define the state name, input and output information in the .json file.
6. Click start execution to test the step function.

## Screenshot Display

![img](lambda1.png)

![img](lambda2.png)

In the input sentence, only "test" is not a stopword. Therefore, the valid number of words of the input text is 1.

![img](step1.png)

![img](step2.png)

![img](step3.png)

## Demo Video

[![Demo Video](demo.mp4)](demo.mp4)
